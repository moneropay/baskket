#!/bin/sh

PGUSER="baskket"
PGPASS="supersecretpass"
PGHOST="localhost:5432"
PGDB="baskket"

atomic=$(printf "%.0f\n" `echo "1000000000000*$3" | bc`)
title=$(echo $1 | sed "s/'/''/g")
desc=$(echo $2 | sed "s/'/''/g")

psql postgresql://$PGUSER:$PGPASS@$PGHOST/$PGDB -c \
	"INSERT INTO products (title, description, price, quantity, image)
	VALUES ('$title', '$desc', $atomic, $4, '$5')"
