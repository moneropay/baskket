document.getElementById("button").addEventListener("click", function(){	
	var id = parseInt(document.getElementById("id").getAttribute("value"))
        var title = document.getElementById("title").getAttribute("value")
        var price = parseFloat(document.getElementById("price").getAttribute("value"))
	var atomic = parseInt(document.getElementById("atomic").getAttribute("value"), 10)

	nOfItems = localStorage.getItem("nitems");
	var local = localStorage.getItem("cart");
	var cart = []
	var found = false

	if (nOfItems == null) {
		localStorage.setItem("nitems", 1)
		displaynItems(1)
	} else {
		localStorage.setItem("nitems", parseInt(nOfItems, 10) + 1);
		displaynItems(parseInt(nOfItems, 10) + 1)
	}

	if (local == null) {
		cart.push({id: id, title: title, price: price,
			atomic: atomic, quantity: 1})
    		localStorage.setItem("cart", JSON.stringify(cart));
		return
	}

	cart = JSON.parse(local)

	cart.forEach((element, index) => {
		if (element.id == id) {
                	element.quantity++
			found = true
		}
	})

	if (found == false) {
		cart.push({id: id, title: title, price: price,
			atomic: atomic, quantity: 1})
	}

	localStorage.setItem("cart", JSON.stringify(cart));
})
